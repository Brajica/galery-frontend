import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Photo} from '../photo/photo.component';
import {PhotoService} from '../photo/photo.service';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss'],
  providers: [PhotoService]
})
export class PhotoListComponent implements OnInit {
  @Input() photo: Photo;
  @Output() success = new EventEmitter();
  @Output() error = new EventEmitter();
  @Output() update = new EventEmitter();
  @Input() type: String;

  constructor(private photoService: PhotoService) {
  }

  ngOnInit() {
  }

  delete() {
    if (this.type !== 'album') {
      this.photoService.deletePhoto(this.photo.id)
        .subscribe(
          res => this.success.emit(),
          error1 => this.error.emit()
        );
    } else {
      this.photo.albumId = 0;
      this.photoService.deletePhotoByAlbum(this.photo.id)
        .subscribe(
          res => this.success.emit(),
          error1 => this.error.emit()
        );
    }

  }


}
