import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PhotoListComponent} from './photo-list.component';
import {MatButtonModule, MatCardModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule
  ],
  declarations: [PhotoListComponent],
  exports: [PhotoListComponent]
})
export class PhotoListModule {
}
