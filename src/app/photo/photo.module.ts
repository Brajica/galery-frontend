import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PhotoRoutingModule} from './photo-routing.module';
import {PhotoComponent} from './photo.component';
import {PhotoListModule} from '../photo-list/photo-list.module';
import {
  MatButtonModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatNativeDateModule,
  MatProgressSpinnerModule, MatSelectModule, MatSnackBarModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PhotoRoutingModule,
    PhotoListModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  declarations: [PhotoComponent]
})
export class PhotoModule {
}
