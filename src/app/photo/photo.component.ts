import {Component, OnInit} from '@angular/core';
import {PhotoService} from './photo.service';
import {NgForm} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {AlbumService} from '../album/album.service';
import {Album} from '../album/album.component';
import * as moment from 'moment';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss'],
  providers: [PhotoService, AlbumService]
})
export class PhotoComponent implements OnInit {
  photos: Array<Photo>;
  albums: Array<Album>;
  photo: Photo;
  add: boolean;
  loadingSave: boolean;
  query: any = {};

  constructor(private photoService: PhotoService,
              public snackBar: MatSnackBar,
              private albumService: AlbumService) {
  }

  ngOnInit() {
    this.getPhotos();
    this.setPhoto();
    this.getAlbums();
  }

  setPhoto() {
    this.photo = {};
  }

  getAlbums() {
    this.albumService.getAllAlbums().subscribe(
      res => this.albums = res,
      error => this.openSnackBar('Ocurrio un erro con los albunes')
    );
  }

  getPhotos() {
    if (this.query.date) {
      this.query.date = moment(this.query.date).format('YYYY-MM-DD');
    }
    this.photoService.getAllPhotos(this.query).subscribe(
      _photos => this.photos = _photos,
      error => this.openSnackBar('Ocurrio un error')
    );
  }

  openSnackBar(message) {
    this.snackBar.open(message, 'Dance', {
      duration: 2000,
    });
  }

  preview(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => {
        this.photo.file = reader.result;
        this.add = true;
      };

      reader.readAsDataURL(file);
    }
  }

  save(form: NgForm) {
    if (form.valid) {
      this.loadingSave = true;
      this.photoService.createPhoto(this.photo).subscribe(
        res => {
          this.getPhotos();
          this.add = false;
          this.loadingSave = false;
          this.setPhoto();
        },
        error => this.openSnackBar('Foto muy pesada')
      );
    }


  }

  updatePhoto() {
    this.photoService.updatePhoto(this.photo, this.photo.id)
      .subscribe(res => {
          this.getPhotos();
          this.add = false;
          this.loadingSave = false;
          this.setPhoto();
        },
        error => this.openSnackBar('ocurrio un error'));
  }


  errorDelete() {
    this.openSnackBar('Intente mas tarde');
  }


}

export interface Photo {
  id?: number;
  name?: string;
  url?: string;
  albumId?: number;
  description?: string;
  updatedAt?: string;
  cratedAt?: string;
  file?: string;
}
