import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Photo} from './photo.component';

@Injectable()
export class PhotoService {

  constructor(private http: HttpClient) {
  }

  getAllPhotos(params?): Observable<Array<Photo>> {
    return this.http.get<Photo[]>(`${environment.api}photo`, {
      params: params,
    }).pipe();
  }

  createPhoto(data): Observable<Photo> {
    return this.http.post(`${environment.api}photo`, data).pipe();
  }

  updatePhoto(data, id): Observable<Photo> {
    return this.http.put(`${environment.api}photo/${id}`, data
    ).pipe();
  }

  deletePhoto(id) {
    return this.http.delete(`${environment.api}photo/${id}`).pipe();
  }

  deletePhotoByAlbum(id): Observable<Photo> {
    return this.http.delete(`${environment.api}photo/delete-album/${id}`).pipe();
  }

}
