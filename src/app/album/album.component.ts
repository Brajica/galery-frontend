import {Component, OnInit} from '@angular/core';
import {Photo} from '../photo/photo.component';
import {PhotoService} from '../photo/photo.service';
import {MatSnackBar} from '@angular/material';
import {AlbumService} from './album.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss'],
  providers: [AlbumService, PhotoService]
})
export class AlbumComponent implements OnInit {
  photo: Photo;
  album: Album;
  albums: Album[];
  photos: Photo[];
  add: boolean;
  loadingSave: boolean;

  constructor(private photoService: PhotoService,
              public snackBar: MatSnackBar,
              private albumService: AlbumService) {
  }

  ngOnInit() {
    this.getPhotos();
    this.setPhoto();
    this.getAlbums();
    this.setAlbum();
  }

  setPhoto() {
    this.photo = {};
  }

  setAlbum() {
    this.album = {};
  }

  getAlbums() {
    this.albumService.getAllAlbums().subscribe(
      res => this.albums = res,
      error => this.openSnackBar('Ocurrio un erro con los albunes')
    );
  }

  getPhotos() {
    this.photoService.getAllPhotos().subscribe(
      _photos => this.photos = _photos,
      error => this.openSnackBar('Ocurrio un error')
    );
  }

  openSnackBar(message) {
    this.snackBar.open(message, 'Dance', {
      duration: 2000,
    });
  }

  preview(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => {
        this.photo.file = reader.result;
        this.add = true;
      };

      reader.readAsDataURL(file);
    }
  }

  save() {
    this.albumService.createAlbum(this.album)
      .subscribe(
        res => {
          this.getAlbums();
          this.setAlbum();
          this.add = false;
          this.loadingSave = false;
        },
        error => this.openSnackBar('Ocurrio un error al crear un album')
      );
  }

  updateAlbum() {
    this.albumService.updateAlbum(this.album, this.album.id)
      .subscribe(
        res => {
          this.getAlbums();
          this.setAlbum();
          this.add = false;
          this.loadingSave = false;
        },
        error => this.openSnackBar('Ocurrio un error al actualizar un album')
      );
  }

}

export interface Album {
  id?: number;
  name?: string;
  description?: string;
  updatedAt?: string;
  createdAt?: string;
}
