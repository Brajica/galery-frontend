import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Album} from './album.component';

@Injectable()
export class AlbumService {

  constructor(private http: HttpClient) {
  }

  getAllAlbums(params?): Observable<Array<Album>> {
    return this.http.get<Album[]>(`${environment.api}album`, {
      params: params,
    }).pipe();
  }

  createAlbum(data): Observable<Album> {
    return this.http.post(`${environment.api}album`, data).pipe();
  }

  updateAlbum(data, id): Observable<Album> {
    return this.http.put(`${environment.api}album/${id}`, data
    ).pipe();
  }

  deletePhotoAlbum(id) {
    return this.http.delete(`${environment.api}album-photo/${id}`).pipe();
  }
}
