import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AlbumRoutingModule} from './album-routing.module';
import {AlbumComponent} from './album.component';
import {
  MatButtonModule,
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule, MatNativeDateModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSnackBarModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import { AlbumDetailComponent } from './album-detail/album-detail.component';
import {PhotoListModule} from '../photo-list/photo-list.module';

@NgModule({
  imports: [
    CommonModule,
    AlbumRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    FormsModule,
    PhotoListModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  declarations: [AlbumComponent, AlbumDetailComponent],
})
export class AlbumModule {
}

