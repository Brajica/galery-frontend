import {Component, OnInit} from '@angular/core';
import {Album} from '../album.component';
import {PhotoService} from '../../photo/photo.service';
import {MatSnackBar} from '@angular/material';
import {AlbumService} from '../album.service';
import {NgForm} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Photo} from '../../photo/photo.component';
import * as moment from 'moment';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss'],
  providers: [PhotoService, AlbumService]
})
export class AlbumDetailComponent implements OnInit {
  photos: Array<Photo>;
  albums: Array<Album>;
  photo: Photo;
  add: boolean;
  loadingSave: boolean;
  albumId: any;
  query: any;

  constructor(private photoService: PhotoService,
              public snackBar: MatSnackBar,
              private albumService: AlbumService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.paramMap.subscribe(params => {

      this.albumId = params.get('id');
      console.log(params, this.albumId);
    });
  }

  ngOnInit() {
    this.query = {
      albumId: Number(this.albumId),
    };
    this.getPhotos();
    this.setPhoto();
    this.getAlbums();

  }


  setPhoto() {
    this.photo = {
      albumId: this.albumId
    };
  }


  getAlbums() {
    this.albumService.getAllAlbums().subscribe(
      res => this.albums = res,
      error => this.openSnackBar('Ocurrio un erro con los albunes')
    );
  }

  getPhotos() {
    if (this.query.date) {
      this.query.date = moment(this.query.date).format('YYYY-MM-DD');
    }
    this.photoService.getAllPhotos(this.query).subscribe(
      _photos => this.photos = _photos,
      error => this.openSnackBar('Ocurrio un error')
    );
  }

  openSnackBar(message) {
    this.snackBar.open(message, 'Dance', {
      duration: 2000,
    });
  }

  preview(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => {
        this.photo.file = reader.result;
        this.photo.albumId = this.albumId;

        this.add = true;
      };

      reader.readAsDataURL(file);
    }
  }

  save(form: NgForm) {
    if (form.valid) {
      this.loadingSave = true;
      this.photo.albumId = Number(this.albumId);
      console.log(this.photo);
      this.photoService.createPhoto(this.photo).subscribe(
        res => {
          this.getPhotos();
          this.add = false;
          this.loadingSave = false;
          this.setPhoto();
        },
        error => this.openSnackBar('Foto muy pesada')
      );
    }


  }

  updatePhoto() {
    this.photoService.updatePhoto(this.photo, this.photo.id)
      .subscribe(res => {
          this.getPhotos();
          this.add = false;
          this.loadingSave = false;
          this.setPhoto();
        },
        error => this.openSnackBar('ocurrio un error'));
  }


  errorDelete() {
    this.openSnackBar('Intente mas tarde');
  }

}



