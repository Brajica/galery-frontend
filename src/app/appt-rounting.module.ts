import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [{
  path: '', children: [
    {path: '', pathMatch: 'full', redirectTo: 'photos'},
    {path: 'photos', loadChildren: './photo/photo.module#PhotoModule'},
    {path: 'albums', loadChildren: './album/album.module#AlbumModule'}
  ]
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ApptRountingModule {
}
